#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <lapacke.h>
#include <time.h>

/* Auxiliary routines prototypes */
double dot(double a[3], double b[3]);
double norm(double v[3]);

/* Parameters */
#define N 3
#define NRHS 1
#define LDA 3
#define LDB 1



extern int perturbedPropagator( double elapsedTime, double *r1, double *v1, double *r2, double *v2 );

int main( int argc,char* argv[] ){
    if (argc != 11){
        printf("Call as:\napplication_C_SinglePerturbedSatellitePropagator tof r1x r1y r1z r2x r2y r2z v1x v1y v1z\n");
    }
    else{
    clock_t start, end;
    double cpu_time_used;
    start = clock();
    // Intiatilize input parameters
    double dtol = 1e-5;
    double TOF = atof(argv[1]);//7724.0;
    double r1[3] = {atof(argv[2]), atof(argv[3]), atof(argv[4])}, *r1ptr = r1; //{7278.14, 0.0, 0.0}
    double r2[3] = {atof(argv[5]), atof(argv[6]), atof(argv[7])}; //{0, 7278.14, 0.0}
    
    
    // Initialize reference velocity
    double vRef[3] = {atof(argv[8]), atof(argv[9]), atof(argv[10])}, *vRefptr = vRef; //0.000160595929422722, 7.40037918524536, 0.0
    int status;

    // Declare variables
    double v1p1[3], *v1p1ptr = v1p1; // Initial velocity of particular solution 1
    double v1p2[3], *v1p2ptr = v1p2; // Initial velocity of particular solution 2
    double v1p3[3], *v1p3ptr = v1p3; // Initial velocity of particular solution 3
    double rRef[3], *rRefptr = rRef; // Final position of reference solution
    double v2[3], *v2ptr = v2; // Final velocity
    double r2p1[3], *r2p1ptr = r2p1; // Final position of particular solution 1
    double r2p2[3], *r2p2ptr = r2p2; // Final position of particular solution 2
    double r2p3[3], *r2p3ptr = r2p3; // Final position of particular solution 3
    int ipiv[N], n = N, nrhs = NRHS, lda = LDA, ldb = LDB, info; // Variables for LAPACKE_dgesv
    double A[3][3], b[3][1];

    //Initialize the reference solution
    // Propagate the reference solution
    status = perturbedPropagator( TOF, r1ptr, vRefptr, rRefptr, v2ptr );
    double diff[3] = {r2[0] - rRef[0], r2[1] - rRef[1], r2[2] - rRef[2]};
    double r2error = norm(diff);
    
    int nIts = 0;
    printf("Iteration = %d, r2error = %f\n",nIts,r2error);
    while(r2error > 1e-5){
        double delta = norm(vRef)*dtol;
        
        // Create first particular solution
        memcpy(v1p1, vRef, sizeof(vRef));
        v1p1[0] = v1p1[0] + delta;      
        status = perturbedPropagator( TOF, r1ptr, v1p1ptr, r2p1ptr, v2ptr );
        
        // Create second particular solution
        memcpy(v1p2, vRef, sizeof(vRef));
        v1p2[1] = v1p2[1] + delta;      
        status = perturbedPropagator( TOF, r1ptr, v1p2ptr, r2p2ptr, v2ptr );
        
        // Create third particular solution
        memcpy(v1p3, vRef, sizeof(vRef));
        v1p3[2] = v1p3[2] + delta;      
        status = perturbedPropagator( TOF, r1ptr, v1p3ptr, r2p3ptr, v2ptr );
        
        /* Solve the equations A*X = b (eq. 13 Woollands et. al)*/
        for (int ii = 0; ii < 3; ii++) {
            A[ii][0] = r2p1[ii] - rRef[ii];
            A[ii][1] = r2p2[ii] - rRef[ii];
            A[ii][2] = r2p3[ii] - rRef[ii];
            b[ii][0] = r2[ii]   - rRef[ii];
        }
              
        info = LAPACKE_dgesv( LAPACK_ROW_MAJOR, n,  nrhs, *A, lda, ipiv, *b, ldb ); // b is replaced with solution X
        /* Check for the exact singularity */
        if( info > 0 ) {
                printf( "The diagonal element of the triangular factor of A,\n" );
                printf( "U(%i,%i) is zero, so that A is singular;\n", info, info );
                printf( "the solution could not be computed.\n" );
                exit( 1 );
        }
        
        // Update reference velocity
        for (int ii = 0; ii < 3; ii++) {
            vRef[ii] = vRef[ii] + delta*b[ii][0];
        }
        
        // Check accuracy of guess
        status = perturbedPropagator( TOF, r1ptr, vRefptr, rRefptr, v2ptr );
        for (int ii = 0; ii < 3; ii++) {
            diff[ii] = r2[ii] - rRef[ii];
        }
        r2error = norm(diff);
        
        // Increment the iteration count
        nIts = nIts + 1;
        printf("Iteration = %d, r2error = %f\n",nIts,r2error);
        if (nIts > 200) {
            printf("Iteration limit exceeded.\n");
            exit(2);
        } else if (r2error > 10000.0){
            printf("Solution is diverging.\n");
            exit(2);
        }
    }
    end = clock();
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;
    printf("Took %f seconds to execute \n", cpu_time_used); 
    printf("Initial velocity solution is v1 = [%f %f %f] km/s\nAfter %d iterations.\n",vRef[0],vRef[1],vRef[2],nIts);
    }
    return 0;
}


// Computes the dot product
double dot(double a[3], double b[3])
{
  double result = a[0]*b[0]+a[1]*b[1]+a[2]*b[2];
  return result;
}
double norm(double v[3]) {
  return sqrt(dot(v,v));
}