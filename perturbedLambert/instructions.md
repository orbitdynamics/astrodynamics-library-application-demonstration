## TUDAT C Perturbed Lambert Solver Application

1. Install the BLAS, Lapack, and Lapacke libraries with the following commands (Ubuntu)
```
    sudo apt install liblapack3
    sudo apt install liblapack-dev 
    sudo apt install libopenblas-base 
    sudo apt install libopenblas-dev 
    sudo apt install liblapacke-dev 
    sudo apt install liblapack-dev
```

2. Download the TUDAT bundle with: `git clone https://github.com/tudat/tudatBundle.git`
2. Copy the perturbedLambert/ directory inside the tudatApplications/ directory.
3. Add `add_subdirectory("${PROJECTROOT}tudatApplications/perturbedLambert/")` to the bottom of the tudatBundle/CMakeLists.txt file.
4. Follow the instructions in tudatBundle/README.md to build the project.
    * The first build of TUDAT will take a long time.
5. If the project was built successfully, then it will generate the tudatBundle/tudatApplications/bin/applications/application_perturbedLambertSolver executable that runs the pertubed Lambert solver.

The application should be called as:  

`./application_perturbedLambertSolver tof r1x r1y r1z r2x r2y r2z v1x v1y v1z`  

where `tof` is the time of flight, `r1_` are the components of the initial position, `r2_` are the components of the final position, and `v1_` are the components of the starting initial velocity guess. The starting initial velocity guess is typically the two body lambert solution that can be retrieved from ivLam. 

The relevant source code for the perturbed Lambert solver is contained in tudatBundle/tudatApplications/perturbedLambert/. The propagator used is based on the singlePerturbedSatellitePropagator example application that come packaged with TUDAT.

